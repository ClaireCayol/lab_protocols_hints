[![GitLab pipeline status](https://gitlab.com/ClaireCayol/labprotocols/badges/master/pipeline.svg)](https://gitlab.com/ClaireCayol/labprotocols/commits/master)

## Introduction
# This is ongoing work directed to members of my past and current research group. 

This project aims to make the lab procedures that I have observed or experimented available to my collaborators and future student for the sake of standardisation, transparency and repeatability.

Future students are encouraged to read the safety procedure explained in the folder "Others" which explains the safety procedures when working with zoonotic pathogens, especially the Puumala hantavirus. 

The project website is available at https://clairecayol.gitlab.io/Research/

## Chapters
The lab protocols are sorted by chapters

• Field: protocols used the field
• Animal Facility: protocols used in the animal facility
• Dirty lab: protocols used in the dirty lab
• DNA: protocols used in DNA lab
• Immunology: protocols of eco-immunology, ELISA
• Safety and miscellaneous: Safety procedures and other protocols

## Folder Usage

If you are a member of my previous or current research group, you will find protocols by clicking on pictures of the portofolio. If you want to use the protocols for your own experiments, the article to be quoted is cited in each protocol.

This project is version controlled via git hosted in GitLab to enable sharing and synchronisation among collaborators.

To have a copy of this project on your computer, you must:

- Have access rights to the GitLab project repository at https://gitlab.com/ClaireCayol/labprotocols

- Have git installed and set up on your computer. If you don't, go to  https://git-scm.com to download it.

If you never used git collaboratively, you will probably have to create an SSH key as described here.

To clone the full project repository, type in Terminal:

```{bash}
git clone git@gitlab.com:ClaireCayol/labprotocols.git
```

To contribute, you can use RStudio or directly GitHub. Remember to branch, commit and request merges with your code.
