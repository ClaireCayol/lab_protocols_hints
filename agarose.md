This note aims at reminding how to make aggarose gel and load DNA template

Needed: 
- gel container
- gel comb
- Eirlenmeyer of 250 ml  


Consummable: 
- Agarose powder
- TA water 1/20
- Buffer 
- ladder
- gloves!! some reagents (Sybr safe) are carcinogenic

PROCEDURE:

# Making agarose gel: 

- Pick size of agarose container and comb depending on the number of samples 
- Prepare if needed the container by closing and taping the open edge of the container
- Place comb at the bottom and/or middle of the container depending on the samples to be loaded
- place the ready empty container under the hoot
- Prepare agarose concentration 1.5% (but because of evaporation we prepare with 125 ml of liquid): 
Mix 125 ml of TA Water (measured with graduated cylinder) and 1.5 g agarose in an erlenmeyer flask of 250 ml. 
Microwave 4 min, then cool down in cold bain-marie
- Under hoot: add 2ul of SYBR safe and gently mix
- Poor into the gel container. Remove bubbles or isolate them on the side if needed (this is a test for how gentle you have been during the previous steps)
- Let it cool down for 15 min. The gel is ready to be used or can be kept in the fridge in a plastic bag

 
# Loading the template:

- centrifuge the DNA plate

- clean the agarose chamber with Deconex if needed

- remove the tape and combs from the gel container (! the comb and tape are contaminated with Sybr safe. Do not let them on the table)

- put the gel in the electrophoresis chamber

- load the buffer (the buffer increases the weight of the samples, allowing them to stay in the bottom of the holes + coloration effect). In JKL, buffer concentration *(5) so 1/5 of the mix should be buffer. The max volume in the holes is 15 ul. 
Advice is (10)-11 ul: (2)-3 ul buffer and 8 ul template
Use reverse pipetting to load the buffer, multipipette can be used if buffer is in appropriate microtubes. It can be good to use paraffine paper to mix buffer and template.
Be aware that the buffer evaporate if tubes are not properly closed after use

- load 8 ul of template in each hole. Sample order should be the same as in the plate, multipipette can be used, first hole of each line is kept empty

- In first hole: introduce ladder that correspond to the fragment size that we are looking for. 10 ul in each hole.

# Plug the electrophoresis system
Switch on 150V and leave it migrate for ~ 1h? 

# Take the picture
! USE GLOVES


