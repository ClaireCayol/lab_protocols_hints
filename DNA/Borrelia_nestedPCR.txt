Based on detection of fla gene

***************
*Described in *
***************


** Wodecka B., Rymaszewska A., Sawczuk M. & Skotarczak B. 2009. Detectability of tick-borne agents DNA in the blood of dogs, undergoing treatment for borreliosis. Ann. Agric. Environ. Med. 16: 9–14.

The assay is not genospecies specific: B. burgdorferi sensu lato complex, B. garinii, B. valaisiana, B. burgdorferi sensu stricto and B. afzelii, and one for B. miyamotoi are amplified with this protocol. 

----> However, by using RFLP on the amplified DNA fragment, it is possible to identify restriction patters specific to genospecies. 

----> Sequencing can also be used for identification of the borrelia genospecies. 


* Wodecka B., Leonska A. & Skotarczak B. 2010. A comparative analysis of molecular markers for the detection and identification of Borrelia spirochaetes in Ixodes ricinus. J. Med. Microbiol. 59: 309–314.

* Wodecka B. 2011. flaB gene as a molecular marker for distinct identification of Borrelia species in environmental samples by the PCR-restriction fragment length polymorphism method. Appl. Environ. Microbiol. 77(19): 7088–7092.


---------------------------------------------------------------------------

JYU local protocol tuned by Sami Kyröläinen:  
---------------------------------------------------------------------------

Step 1: [Outer primers]

For 1 samples: 

H20 		6.05 ul
Buffer 		1 ul 		(Thermo dreamTaq Buffer)
dNTP		1 ul
BSA		0.5 ul
132F		0.2 ul (10mM)
905R		0.2 ul (10mM)
DreamTaq	0.05 ul		(Thermo)
DNA		1 ul
	_____________________	
TOT		10 ul




Parameters: 	95 deg 
		95 deg
		50 deg 
		72 deg 
		72 deg 
40 cycles

Size of the amplification product = 774 bp


		


Step 2: [Inner primers]

H20		6.05 ul
Buffer 		1 ul
dNTP		1 ul
BSA		0.5 ul
220F		0.2 ul (10mM)
823R		0.2 ul (10mM)
DreamTaq	0.05 ul
DNA 		1 ul
	____________________
TOT		10 ul



!! Do not vortex DreamTaq


Parameters: 	95 deg 
		95 deg
		54 deg **
		72 deg 
		72 deg 


Size of the amplification product = 604 bp = FINAL SIZE

NB: Normally it is better to reduce the amount of primer in the second reaction



---------------------------------------------------------------------------

JYU local protocol tuned by Sami Kyröläinen when sequencing is planned: 
---------------------------------------------------------------------------

First step occurs same as described before


Second step is run longer when sequencing is planned. 

Recipe Step 2 is changed to:

H20		12.1 ul up to 12.5 ul
Buffer 		2 ul
dNTP		2 ul
BSA		1 ul
220F		0.4 ul (10mM) (we also did it with 0.2 ul and was fine)
823R		0.4 ul (10mM) (we also did it with 0.2 ul and was fine)
DreamTaq	0.1 ul
DNA 		2 ul






		